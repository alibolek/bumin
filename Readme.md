# Bumin Homework Reporting API

Sample project for Bumin homework

## Getting Started

Following instructions will get you a copy of the project up and running
 on your local machine for development and testing purposes.

### Manual Installation

* Download web2py framework;

```
~# wget https://mdipierro.pythonanywhere.com/examples/static/web2py_src.zip
```

* Go to web2py_src.zip directory and extract it.

```
~# cd <Downloads>/
~# unzip web2py_src.zip
~# cd web2py/applications
```

* Install requests package

```
~# sudo pip install requests
```

* Clone this project to this directory

```
~# git clone git@gitlab.com:alibolek/bumin.git
~# cd bumin/
~# mv routes.py ../../routes.py | cd ../../
```

* Run web2py and done!

```
~# python web2py.py
```

## Deployment

This project deployed on pythonanywhere.com


## Authors

* **Ali Bölek** - *Owner* - [Gitlab](https://gitlab.com/alibolek)
