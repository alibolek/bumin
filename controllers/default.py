# -*- coding: utf-8 -*-
import requests


def get_token():
    global r, token, status
    payload = {'email': "demo@bumin.com.tr", 'password': "cjaiU8CV"}
    r = requests.post("https://sandbox-reporting.rpdpymnt.com/api/v3/merchant/user/login", data=payload)
    token = r.json()['token']
    session.myvariable = token
    status = r.json()['status']
    redirect(URL('index', args="token_created"))


def index():
    session.flash = T('yes yes')
    return locals()


def pages():
    links = ["report", "list", "transaction", "client"]
    return locals()


def page():
    global token
    headers = {'Authorization': session.myvariable}
    if request.args[0] == "report":
        payload = {'toDate': request.vars['toDate'], 'fromDate': request.vars['fromDate']}
        p = requests.post("https://sandbox-reporting.rpdpymnt.com/api/v3/transactions/report",
                          data=payload, headers=headers)
        responsed = p.json()['response']
    elif request.args[0] == "list":
        payload = {'fromDate': request.vars['fromDate'],
                   'toDate': request.vars['toDate'],
                   'status': request.vars['status'],
                   'operation': request.vars['operation'],
                   'merchantId': request.vars['merchantId'],
                   'acquirerId': request.vars['acquirerId'],
                   'paymentMethod': request.vars['paymentMethod'],
                   'errorCode': request.vars['errorCode'],
                   'filterField': request.vars['filterField'],
                   'filterValue': request.vars['filterValue'],
                   'page': request.vars['page']}
        p = requests.post("https://sandbox-reporting.rpdpymnt.com/api/v3/transaction/list",
                          data=payload, headers=headers)
        responsed = p.json()
    elif request.args[0] == "transaction":
        payload = {'transactionId​': request.vars['transactionId'], 'uuid': request.vars['uuid']}
        p = requests.post("https://sandbox-reporting.rpdpymnt.com/api/v3/transaction",
                          data=payload, headers=headers)
        responsed = p.json()

    elif request.args[0] == "client":
        payload = {'transactionId​': request.vars['transactionId']}
        p = requests.post("https://sandbox-reporting.rpdpymnt.com/api/v3/client",
                          data=payload, headers=headers)
        responsed = p.json()

    return locals()


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
